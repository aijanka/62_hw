import React from 'react';
import {Grid, Panel} from "react-bootstrap";
import './Contacts.css';

const Contacts = props => {
    const FontAwesome = require('react-fontawesome');

    return (
        <Grid>
            <div className="Contacts">
                <Panel className='contactPanel'>
                    <Panel.Heading><Panel.Title componentClass="h3">Наш адрес</Panel.Title></Panel.Heading>
                    <Panel.Body>{props.contacts.address}</Panel.Body>
                </Panel>
                <Panel className='contactPanel'>
                    <Panel.Heading><Panel.Title componentClass="h3">Номера телефонов</Panel.Title></Panel.Heading>
                    {props.contacts.phones.map(contact => <Panel.Body>{contact.phoneNum}   -    {contact.name}</Panel.Body>)}
                </Panel>
                <Panel className='contactPanel'>
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">Соцсети и почта</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>{props.contacts.email}</Panel.Body>
                    <Panel.Body>
                        {props.contacts.socBtns.map(soc => <a href={soc.link}><FontAwesome className={`fa ${soc.iconName} socBtn`}/></a>)}
                    </Panel.Body>

                </Panel>
            </div>
        </Grid>

    )
};

export default Contacts;