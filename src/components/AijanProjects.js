import React, {Fragment} from 'react';
import {ListGroup, ListGroupItem} from "react-bootstrap";
import App from './Projects/spravochnik/App.js';
import {NavLink, Route, Switch} from "react-router-dom";


const AijanProjects = () => {
    return (
        <Fragment>
            <ListGroup>
                <ListGroupItem><NavLink to='/spravochnik'>Справочник</NavLink></ListGroupItem>
            </ListGroup>
            <Switch>
                <Route path='/sprabochnik' component={App}/>

            </Switch>
        </Fragment>

    )
};

export default AijanProjects;