import React from 'react';
import {Grid} from "react-bootstrap";
import './Platforms.css';

const Platforms = props => {
    const FontAwesome = require('react-fontawesome');
    return (
        <Grid>
            <div className="platformsContainer">
                {props.platforms.map(platform => (
                    <div className="platform">
                        <FontAwesome
                            className={`fa ${platform.className}`}
                            name=''
                            size='5x'
                            spin
                            style={{textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
                        />
                        <h2>{platform.heading}</h2>
                    </div>
                    )
                )}
            </div>
        </Grid>
    )
};

export default Platforms;
