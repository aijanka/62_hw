import React, {Component, Fragment} from 'react';
import axios from 'axios';
import './countriesList.css';
import CountryDescription from "./CountryDescription/CountryDescription";

class App extends Component {

    state = {
        countriesList: [],
        countryDescription: '',
        borderCountries: []
    };

    componentWillMount() {
        axios.get('/all')
            .then(response => this.setState({countriesList: response.data}));
    }

    convertCodesToNames = borders => {
        if (borders.length !== 0) {
            // borders.map(countryCode => {
            //     axios.get('/alpha/' + countryCode).then(response => this.setState({borderCountries: response.data.name}));
            //     console.log(borders);
            // });
            Promise.all(borders.map(bcode => axios(`/alpha/${bcode}`)))
                .then(response => {
                    const borderCountries = response.map(r => r.data.name);
                    this.setState({borderCountries})
                })

        }
        else this.setState({borderCountries: 'no borders'})

    };

    showCountryDescription(name) {
        axios.get('/name/' + name).then(response => {
            const c = response.data[0];

            this.convertCodesToNames(c.borders);
            this.setState({
                countryDescription: {
                    name: c.name,
                    capital: c.capital,
                    population: c.population,
                }
            })
        });
    }

    render() {
        const chooseCountryDiv = <p className='chooseCountry'>Choose the country :) </p>;
        if (this.state.countriesList.length === 0) {
            return <div>Loading...</div>
        }
        else {
            return (
                <Fragment>
                    <div className="countriesList">
                        {this.state.countriesList.map((c, index) => {
                            return (<p onClick={() => this.showCountryDescription(c.name)} key={index}>{c.name}</p>)
                        })}
                    </div>
                    <div className="countryDescription">
                        { this.state.countryDescription ? (<CountryDescription
                            name={this.state.countryDescription.name}
                            capital={this.state.countryDescription.capital}
                            population={this.state.countryDescription.population}
                            border={this.state.borderCountries}
                        />) : chooseCountryDiv}
                    </div>
                </Fragment>
            );
        }
    }
}
// Promise.all(borders.map(b => axios.get(`/alpha/${b}`)))
//     .then(res => {
//         const borderCountries = res.map(r => r.data.name);
//         this.setState({borderCountries})
//     })

export default App;
