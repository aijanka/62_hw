import React, {Component, Fragment} from 'react';
import Wrapper from "./hoc/Wrapper";
import './navbar.css'
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {NavLink, Route, Switch} from "react-router-dom";
import AboutUs from "./components/AboutUs";
import Platforms from "./components/Platforms/Platforms";
import Contacts from "./components/Contacts/Contacts";
import AijanProjects from './components/AijanProjects';

class App extends Component {
    state = {
        platforms: [
            {className: 'fa-apple', heading: 'IOS'},
            {className: 'fa-android', heading: 'Android'},
            {className: 'fa-laptop', heading: 'Back end'},
            {className: 'fa-code', heading: 'Front-end'},
            {className: 'fa-pencil-square', heading: 'UI-UX design'}
        ],
        contacts: {
            address: 'Some address on Some Street',
            phones: [
                    {name: 'Sanira', phoneNum: '0778 78 78 78'},
                    {name: 'Begimai', phoneNum: '0778 78 78 78'}
                    ],
            email: 'some@mail.com',
            socBtns: [
                    {link: 'fb.com/blablabla', iconName: 'fa-facebook'},
                    {link: 'linkedin.com/blablabla', iconName: 'fa-linkedin'}
                    ]
        }
    };

    render() {
        return (
            <Fragment>
                <Navbar inverse collapseOnSelect className='navbar'>
                    <Navbar.Header><Navbar.Brand><a href="#brand">NEOBIS</a></Navbar.Brand><Navbar.Toggle/></Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            <NavItem><NavLink to='/aboutUs'>О нас</NavLink></NavItem>
                            <NavItem><NavLink to='/platforms'>Платформы</NavLink></NavItem>
                            <NavItem><NavLink to='/contacts'>Контакты</NavLink></NavItem>
                            <NavItem><NavLink to='/aijanProjects'>Проекты Айжанки</NavLink></NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Switch>
                    <Route path='/aboutUs' component={AboutUs}/>
                    <Route path='/platforms' render={() => <Platforms platforms={this.state.platforms}/>}/>
                    <Route path='/contacts' render={() => <Contacts contacts={this.state.contacts}/>}/>
                    <Route path='/aijanProjects' component={AijanProjects}/>

                </Switch>
            </Fragment>
        );
    }
}

export default App;
